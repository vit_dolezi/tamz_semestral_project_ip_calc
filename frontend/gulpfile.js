var gulp = require('gulp');
var uglify = require('gulp-uglify');
var gulpsync = require('gulp-sync')(gulp);
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var mainBowerFiles = require('main-bower-files');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var clean = require('gulp-clean');
var inject = require('gulp-inject');
var es = require('event-stream');


gulp.task('default', ['sync', 'watch']);

gulp.task('scriptsAndIndex', function () {
    var appStream = gulp.src('src/app/**/*.js')
        .pipe(gulp.dest('dist/app'));

    gulp.src('./src/index.html')
        .pipe(inject(es.merge(appStream),  {ignorePath: 'dist', addRootSlash: false } ))
        .pipe(gulp.dest('./dist'))
        .pipe(reload({stream: true}));
});

gulp.task('scripts', function () {
    gulp.src('src/app/**/*.js')
        .pipe(gulp.dest('dist/app'))
        .pipe(reload({stream: true}))


});

gulp.task('html', function () {
    gulp.src('src/app/**/*.html')
        .pipe(gulp.dest('dist/app'))
        .pipe(reload({stream: true}));
});


gulp.task('styles', function () {
    gulp.src('src/**/*.css')
        .pipe(cleanCSS())
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('dist'))
        .pipe(reload({stream: true}));
});

gulp.task('others', function () {
    gulp.src(['src/**/*.ico','src/**/*.ogx','src/**/*.ogg'])
        .pipe(gulp.dest('dist'));
});


gulp.task('watch', function () {
    gulp.watch('src/app/**/*.js', ['scripts']);
    gulp.watch('src/index.html', ['scriptsAndIndex']);
    gulp.watch('src/**/*.css', ['styles']);
    gulp.watch('src/app/**/*.html', ['html']);
});

gulp.task('index',function(){
    var target = gulp.src('./src/index.html');
    var sources = gulp.src(['./src/app/**/*.js'], {read: false});

    target.pipe(inject(sources, {ignorePath: 'src', addRootSlash: false }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('browser-sync', function () {
    browserSync({
        server:{
            baseDir:"./dist"
        }
    })
});

gulp.task('bowerFilesJs', function () {
    gulp.src(mainBowerFiles('**/*.js'))
    //.pipe(uglify())
        .pipe(concat('bowerAll.js'))
        .pipe(gulp.dest('dist'));
});


gulp.task('bowerFilesCss', function () {
    gulp.src(mainBowerFiles('**/*.css'))
    //.pipe(cleanCSS())
        .pipe(concat('bowerAll.css'))
        .pipe(gulp.dest('dist'));
});

gulp.task('clean',function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

gulp.task('sync', gulpsync.sync([
    ['clean'],
    ['scriptsAndIndex', 'styles', 'html', 'bowerFilesJs', 'bowerFilesCss', 'others'],
    ['browser-sync']]
));
