angular.module('IpCalc.domain.Csv', [
])
    .service('Csv', function () {
        return {
                csvExport: function(objId, data){
                    var a = document.getElementById(objId);
                    var finalCsv = '';

                    _.forEach(data, function (item) {
                        finalCsv += item.ipLowStr + ',' + item.prefixSize + ',' + item.prefixMaskStr + '\n';
                    });

                    var file = new Blob([finalCsv], {type: 'text/plain'});
                    a.href = URL.createObjectURL(file);
                    a.download = "ip_export.csv";
                }
            }
    });