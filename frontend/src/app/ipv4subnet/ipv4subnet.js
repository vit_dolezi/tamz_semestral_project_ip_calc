angular.module('IpCalc.ipv4subnet',
    [
        'ngRoute'
    ])

    .config(['$routeProvider', function( $routeProvider ) {
        $routeProvider.when('/ipv4subnet', {
            templateUrl: 'app/ipv4subnet/ipv4subnet.tpl.html',
            controller: 'ipSubnetsCtrl'
        });

    }])

    .controller('ipSubnetsCtrl', function( $scope, $rootScope, IpCalculator, Csv) {
        $rootScope.dynamicTitle = "IPv4 Subnet Calculator";
        $scope.ipv4Pattern = IpCalculator.ipv4Pattern;
        $scope.csvExport = function(){
            document.getElementById('emp').play();
            Csv.csvExport('csv',$scope.output);
        };




        $scope.ipInput={
        };

        if (sessionStorage.ipAddressFromSubnet) {
            $scope.ipInput.ipAddressFrom = sessionStorage.ipAddressFromSubnet;
        }

        if (sessionStorage.ipAddressToSubnet) {
            $scope.ipInput.ipAddressTo = sessionStorage.ipAddressToSubnet;
        }

        if (sessionStorage.prefixLengthSubnet) {
            $scope.ipInput.prefixLength = Number(sessionStorage.prefixLengthSubnet);
        }else{
            $scope.ipInput.prefixLength = 24;
        }

        $scope.output = [];

        $scope.inputChange = function(){
            if(!( IpCalculator.isIp($scope.ipInput.ipAddressFrom) && IpCalculator.isIp($scope.ipInput.ipAddressTo))){
                return;
            }

            $scope.output = IpCalculator.calculateSubnets($scope.ipInput.ipAddressFrom, $scope.ipInput.ipAddressTo, $scope.ipInput.prefixLength);

            sessionStorage.ipAddressFromSubnet = $scope.ipInput.ipAddressFrom;
            sessionStorage.ipAddressToSubnet = $scope.ipInput.ipAddressTo;
            sessionStorage.prefixLengthSubnet = $scope.ipInput.prefixLength;
        };

        $scope.inputChange();


    });