angular.module('IpCalc.ipv4range',
    [
        'ngRoute'
    ])

    .config(['$routeProvider', function( $routeProvider ) {
        $routeProvider.when('/ipv4range', {
            templateUrl: 'app/ipv4range/ipv4range.tpl.html',
            controller: 'ipRangeCtrl'
        });

    }])

    .controller('ipRangeCtrl', function( $scope, $rootScope, IpCalculator, Csv) {
        $rootScope.dynamicTitle = "IPv4 Range to Prefix Converter";
        $scope.ipv4Pattern = IpCalculator.ipv4Pattern;
        $scope.csvExport = function(){
            document.getElementById('emp').play();
            Csv.csvExport('csv',$scope.output);
        };


        $scope.ipInput={
        };


        if (sessionStorage.ipAddressFromRange) {
            $scope.ipInput.ipAddressFrom = sessionStorage.ipAddressFromRange;
        }

        if (sessionStorage.ipAddressToRange) {
            $scope.ipInput.ipAddressTo = sessionStorage.ipAddressToRange;
        }



        $scope.output = [];



        $scope.inputChange = function(){
            if(!( IpCalculator.isIp($scope.ipInput.ipAddressFrom) && IpCalculator.isIp($scope.ipInput.ipAddressTo))){
                return;
            }
            $scope.output = IpCalculator.calculate($scope.ipInput.ipAddressFrom, $scope.ipInput.ipAddressTo);
            sessionStorage.ipAddressToRange = $scope.ipInput.ipAddressTo;
            sessionStorage.ipAddressFromRange = $scope.ipInput.ipAddressFrom;
        };

        $scope.inputChange();


    });