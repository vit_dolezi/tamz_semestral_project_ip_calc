angular.module("IpCalc", [

    'ngRoute',
    'ngMaterial',
    'ngMessages',
    'IpCalc.domain.IpCalculator',
    'IpCalc.ipv4address',
    'IpCalc.ipv4range',
    'IpCalc.ipv4subnet',
    'IpCalc.about',
    'IpCalc.ipv6subnet',
    'IpCalc.domain.Csv'
])

    .run(function(  $route, $location) {
            $location.url('/ipv4address');
    })
    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('light-blue')
            .accentPalette('blue');
    })
    .controller('AppController', function($rootScope, $scope, $mdSidenav, $timeout) {
        $rootScope.dynamicTitle = "BTC";

        if(!localStorage.enableSound){
            localStorage.enableSound = "1";
        }


        $scope.pages = [
            {href:"/ipv4address", name:"IPv4 Address Analyzer"},
            {href:"/ipv4range", name:"IPv4 Range to Prefix Converter"},
            {href:"/ipv4subnet", name:"IPv4 Subnet Calculator"},
            {href:"/ipv6subnet", name:"IPv6 Subnet Calculator"},
            {href:"/about", name:"About"}
        ];

        $scope.toggleLeft = buildDelayedToggler('left');

        function debounce(func, wait, context) {
            var timer;
            return function debounced() {
                var context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function() {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }
        function buildDelayedToggler(navID) {
            return debounce(function() {
                $mdSidenav(navID).toggle();
            }, 200);
        }
        function buildToggler(navID) {
            return function() {
                $mdSidenav(navID).toggle();
            };
        }

        $scope.close = function() {
            $mdSidenav('left').close();
        };

    });
