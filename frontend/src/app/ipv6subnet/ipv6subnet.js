angular.module('IpCalc.ipv6subnet',
    [
        'ngRoute'
    ])

    .config(['$routeProvider', function( $routeProvider ) {
        $routeProvider.when('/ipv6subnet', {
            templateUrl: 'app/ipv6subnet/ipv6subnet.tpl.html',
            controller: 'ipv6SubnetsCtrl'
        });

    }])

    .controller('ipv6SubnetsCtrl', function( $scope, $rootScope) {
        $rootScope.dynamicTitle = "IPv6 Subnet Calculator";

        $scope.ipInput={
            // ipAddress: "2001:0DB8:ABCD:0012:0000:0000:0000:0001",
            prefixLength: 64
        };

        if (sessionStorage.ipv6addressip) {
            $scope.ipInput.ipAddress = sessionStorage.ipv6addressip;
        }

        $scope.output = {};

        var IpRange = function (ip){


            var splited = ip.split(':');
            var fromString = '';
            var toString = '';

            for(var i = 0; i < 4; i++){
                fromString += splited[i] + ':';
            }
            toString = fromString;

            return {
                network: fromString + ':',
                range:{
                    from: fromString+'0000:0000:0000:0000',
                    to: toString+'FFFF:FFFF:FFFF:FFFF'
                }
            }
        };

        $scope.inputChange = function(){
            var expression = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/;
            if(!expression.test($scope.ipInput.ipAddress)){
                return;
            }

            $scope.output = {};
            $scope.output = IpRange($scope.ipInput.ipAddress);
            $scope.output.totalIpAddresses = Math.pow(2, $scope.ipInput.prefixLength);

            sessionStorage.ipv6addressip = $scope.ipInput.ipAddress;
        };

        $scope.inputChange();




    });