angular.module('IpCalc.about',
    [
        'ngRoute'
    ])

    .config(['$routeProvider', function( $routeProvider ) {
        $routeProvider.when('/about', {
            templateUrl: 'app/about/about.tpl.html',
            controller: 'aboutCtrl'
        });

    }])

    .controller('aboutCtrl', function( $scope, $rootScope) {


        if(localStorage.enableSound === "1"){
            document.getElementById('horse').play();
            $scope.enableSound = true;
        }else{
            $scope.enableSound = false;
        }



        $scope.onChange = function(){
            if($scope.enableSound){
                localStorage.enableSound = "1";
            }else{
                localStorage.enableSound = "0";
            }


        };


        $rootScope.dynamicTitle = "About";



    });
