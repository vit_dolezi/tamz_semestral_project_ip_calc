angular.module('IpCalc.ipv4address',
    [
        'ngRoute'
    ])

    .config(['$routeProvider', function( $routeProvider ) {
        $routeProvider.when('/ipv4address', {
            templateUrl: 'app/ipv4address/ip.tpl.html',
            controller: 'ipCtrl'
        });

    }])

    .controller('ipCtrl', function( $scope, $rootScope, IpCalculator) {
        $rootScope.dynamicTitle = "IPv4 Address Analyzer";
        $scope.ipv4Pattern = IpCalculator.ipv4Pattern;

        $scope.ipInput={
            prefixLength: 24
        };

        if (sessionStorage.ipv4addressip) {
            $scope.ipInput.ipAddress = sessionStorage.ipv4addressip;
        }

        if (sessionStorage.prefixLength) {
            $scope.ipInput.prefixLength = Number(sessionStorage.ipv4addressipprefixLength);
        }



        $scope.output = {
        };

        $scope.numberOfSubnets = function(prefixLength){
            if(prefixLength){
                return Math.pow(2, prefixLength);
            }
        };

        $scope.numberOfSubnets = function(prefixLength){
            if(prefixLength){
                return Math.pow(2, prefixLength);
            }
        };

        $scope.inputChange = function(){
            if(!( IpCalculator.isIp($scope.ipInput.ipAddress))){
                return;
            }

            setOutputs();

            $scope.ipInput.networkMask = $scope.output.prefixMaskStr;

        };

        $scope.inputMaskChange = function(){
            if(!( IpCalculator.isIp($scope.ipInput.ipAddress) && IpCalculator.isIp($scope.ipInput.networkMask))){
                return;
            }

            $scope.ipInput.prefixLength = IpCalculator.getPrefixSize(  IpCalculator.toDecimal( $scope.ipInput.networkMask));

            setOutputs();
        };

        var setOutputs = function () {
            $scope.output = IpCalculator.calculateSubnetMask($scope.ipInput.ipAddress, $scope.ipInput.prefixLength);
            $scope.output.orgInput = $scope.ipInput.ipAddress;

            sessionStorage.ipv4addressip = $scope.ipInput.ipAddress;
            sessionStorage.ipv4addressipprefixLength = $scope.ipInput.prefixLength;
        };


        $scope.inputChange();


    })
    .filter('ipToBin', function () {
        return function (ip) {
            try{
                var ipParts = ip.split(".");
            }catch(e){
                return;
            }
            var finalString = '';

            _.forEach(ipParts,function(ipPart){
                finalString += ('0000000'+(Number(ipPart)).toString(2)).slice(-8) + ".";
            });

            return finalString.substring(0,finalString.length-1);
        };
    })
    .filter('ipToHex', function () {
        return function (ip) {
            try{
                var ipParts = ip.split(".");
            }catch(e){
                return;
            }
            var finalString = '';

            _.forEach(ipParts,function(ipPart){
                finalString += ('00'+(ipPart >>> 0).toString(16)).slice(-2) + ".";
            });

            return finalString.substring(0,finalString.length-1).toUpperCase();
        };
    });
